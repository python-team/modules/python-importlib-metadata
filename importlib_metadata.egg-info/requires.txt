zipp>=3.20

[:python_version < "3.8"]
typing-extensions>=3.6.4

[check]
pytest-checkdocs>=2.4

[check:sys_platform != "cygwin"]
pytest-ruff>=0.2.1

[cover]
pytest-cov

[doc]
sphinx>=3.5
jaraco.packaging>=9.3
rst.linker>=1.9
furo
sphinx-lint
jaraco.tidelift>=1.4

[enabler]
pytest-enabler>=2.2

[perf]
ipython

[test]
pytest!=8.1.*,>=6
packaging
pyfakefs
flufl.flake8
pytest-perf>=0.9.2
jaraco.test>=5.4

[test:python_version < "3.9"]
importlib_resources>=1.3

[type]
pytest-mypy
